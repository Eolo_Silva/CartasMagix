﻿namespace JogoCartasMagix
{
    public abstract class Carta
    {
        public string Nome { get; set; }
        public abstract int Custo { get; set; }
        public string Cor { get; set; }
        public string Imagem { get; set; }
        public string LinhaDeTipo { get; set; }
        public string SimboloExpansao { get; set; }
        public string Descricao { get; set; }
      

        public Carta(string nome, int custo, string cor, string imagem, string linhadetipo, string simboloexpansao, string descricao)
        {
            
            Nome = nome;
            Custo = custo;
            Cor = cor;
            Imagem = imagem;
            LinhaDeTipo = linhadetipo;
            SimboloExpansao = simboloexpansao;
            Descricao = descricao;

           
        }

        public override string ToString()
        {
            return $"Nome : {Nome}\nCusto : {Custo}\nCor : {Cor}\nImagem : {Imagem}\nLinha de Tipo : {LinhaDeTipo}\nSimbolo de Expansão : {SimboloExpansao}\nDescricao : {Descricao}\nPoder : {Poder}\nResistencia: {Resistencia}";
        }
        public abstract string ExibeCarta();

    }
}


