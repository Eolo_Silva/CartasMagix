﻿using System;

namespace JogoCartasMagix
{
    public class Criatura : Carta
    {

        public int Poder { get; set; }
        public int Resistencia { get; set; }
        public int _custo { get; set; }

        public override int Custo
        {
            get
            {
                return _custo;
            }

            set
            {
                _custo = 0;
            }
        }
        public Criatura(string nome, int custo, string cor, string imagem, string linhadetipo, string simboloexpansao, string descricao, int poder, int resistencia) : base(nome, custo, cor, imagem, linhadetipo, simboloexpansao, descricao)
        {
            
            Poder = poder;
            Resistencia = resistencia;
            _custo = custo;
        }
        public override string ToString()
        {
            return $"Carta Magic -> {base.ToString()} , Poder/Resistência : {ExibeCarta()}";
        }
        public override string ExibeCarta()
    {
        throw new NotImplementedException();
    }
}
}
